This is a solidity coding challenge. The goal is to run

```truffle test```

and have the test pass. There is one contract in the repository, Insecure01.sol. It has a security hole. Please write a contract Exploit01.sol that exploits it and passes the test in test/exploit.js by withdrawing double what you deposit. Please put your response in a private repository to avoid giving the answers away to other participants.

You'll have to npm install truffle and ethereumjs-testrpc.

Happy hacking
